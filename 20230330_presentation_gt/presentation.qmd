---
title: "Présentation du gt notebook"
format: 
  revealjs:
    logo: "img/2023_logo_GT_notebook_dark_version_01.png"
    footer: gt notebook - Webinaire 1 - 30 mars 2023
    incremental: false 
editor: visual
---

## Ordre du jour

-   accueil
-   présentation du gt
-   introduction à Quarto par Nicolas Roelandt
-   retour d'expérience Packaging de Quarto dans Guix par Sébastien Rey-Coyrehourcq
-   revue d'article par Raphaëlle Krummeich
-   discussions avec le public et les intervenant-es
-   prochains Webinaires :
    -   l'interactivité du notebook, 13 avril
    -   discussions & co-construction

## Webinaire collaboratif du gt notebook

[gitlab du webinaire](https://gitlab.huma-num.fr/gt-notebook/webinaires/-/issues/2){preview-link="true"} ![](img/Webinaire_1_Tickets%20_GT-Notebook_Webinaires_GitLab.png)

## Présentation du GT

::: {.absolute top="200"}
-   genèse

-   manifeste

-   objectifs
:::

## Genèse

::: {.absolute top="200"}
construire en interdisciplinarité

-   un atelier reproductible pour la sensibilisation aux Notebooks,

-   un objet complexe dans le cycle de vie de la donnée.
:::

## Manifeste \[WIP\]

::: {.absolute top="200"}
Nous sommes vigileant·es à ce que ce lieu soit et reste inclusif de toutes les communautés, de toutes les pratiques, de toutes les initiatives, dès lors qu'elles respectent quelques règles communes. Celles-ci ne vous surprendront pas si venez de l'ESR. Nous réflechissons à l'écriture d'un manifeste du gt.
:::

## Objectifs 1/3

::: {.absolute top="200"}
Interdisciplinarité : parce que cela se construit dans le dialogue et dans la durée :)
:::

## Objectifs 2/3

::: {.absolute top="100"}
Axes de réflexions et de veille (WIP) autour du Notebook

-   épistémologie de l'objet et des pratiques autour de l'objet
-   écosophie, un engagement réflexif et critique vis à vis des pratiques du numérique
-   litératie(s) numérique(s), pour (re)-penser l'écriture (enjeux, pratiques, etc.) au prisme de la machine et de son contexte
-   reproductibilité, comme un axe transversal de réflexion et horizon
:::

## Objectifs 3/3

Mise en oeuvre au sein d'actions, conçues et portées par les volontés des membres du gt :

-   liste de diffusion
-   sous-groupe(s) par axe d'intérêt
-   webinaires d'une heure mensuel, compte-rendus et enregistrement
-   opérationalisation
    -   veille sur les outils, les plateformes,
    -   élaboration d'une base de connaissance
    -   réalisation de supports et d'ateliers de sensibilisation reproductibles
-   \[WIP\] une journée annuelle de discussions/retranscriptions des GT ?

## Introduction à Quarto

::: {.absolute top="200"}
::: {style="text-align:center"}
-   Intervention sur *Quarto, système open source de publication scientifique et technique basé sur Pandoc*, par Nicolas Roelandt

-   Retour d'expérience *packaging guix de Quarto*, par Sébastien Rey-Coreyhourcq
:::
:::
