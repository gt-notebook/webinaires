
#Ordre du Jour Webinaire XX

**Durée** : 45 min max

**Date** : 

**Membre du GT Animateur** : 

**Programme :**

- Revue de littérature, 10 min en mode j'ai vu, j'ai fait, j'ai lu, etc.

  - *[NomDuPresentateur] [Sujet]*

- 1 présentation de 15 min
  
  - *[NomDuPresentateur] [Sujet]*

- 1 discussion de 10 minute sur la présentation

- 1 discussion de 10 minute générale

  - *[Sujet à aborder]*
  - Discussion sur le prochaine Webinaire

